//###########################################################################################################
/** @file	LRUcache.cpp
	@brief	Fichero fuente de la clase LRUCache, la cual est� definida en el fichero LRUcache.h
	@author	PATRICIO J. MART�NEZ
	@date	26/01/2020
*/ //########################################################################################################
#include "lrucache.h"

// Los CONSTRUCTORES y los DESTRUCTORES es mejor definirlos en el .h para evitar durante el linkado problema de tipo --"undefined reference to XXX::XXX(X)"--

//=======================================================================================================
	/** @fn			set(int* key, int* value)
		@brief		Actualiza la cach� con los valores introducidos.
					Si la key existe en la cach�, actualiza su el value asociado a ella.
					Si la key no exist�a en cach�, la incluye con su value asociado, posicion�ndola como la usada m�s recientemente. Y si la cach� estaba llena, eliminar� el posicionado como usado menos recientemente.
		@return		Devuelve:
					<ol>
						<li>[true] = Operaci�n realizada correctamente</li>
						<li>[false] = Operaci�n no realizada correctamente</li>
					</ol>
*/ //====================================================================================================
bool LRUCache::set(int* key, int* value)
{

	bool result = false;
	struct nodoLista* aux = head;
	int nodosOcupados = 0;
	// Si hay datos en la cach�...
	if (head != NULL)
	{
		// Recorremos uno a uno los nodos buscando la "key"...
		do
		{	// Si encontramos la "key"...
			if (aux->key == *key)
			{	// Actualizamos su "value" asociado
				aux->value = *value;
				// Hemos procesado el comando correctamente
				result = true;
				break;
			}
			//Sino, pasamos al siguiente nodo de la cach� para buscar all�
			aux = aux->pos;
			nodosOcupados++;
		}while (aux != NULL);
	}
	
	// Si no se encontr� la key en la lista de cach� y queda espacio para almacenar nuevos nodos...
	if (result == false && nodosOcupados < cp)
	{	
		// Reservamos espacio para almacenar un nuevo nodo
		aux = (nodoLista*)malloc(sizeof(nodoLista));
		
		// Completamos el nuevo nodo con la info correspondiente (para posicionarlo a la cabeza de la lista de cach�)
		aux->pre 		= NULL;
		aux->key		= *key;
		aux->value 		= *value;
		// Si no se trata del primer nodo que introducimos en cach�...
		if (head != NULL)
		{	// Apuntamos con la anterior cabecera a este y viceversa
			aux->pos	= head;
			head->pre	= aux;
		}
		// Si se trata del primer nodo que introducimos...
		else
		{ 	// Ahora ser� tambi�n la cola de la lista de cach�
			aux->pos	= NULL;
			tail	= aux;
		}
		// Ya lo podemos convertir en la cabeza de la lista de cach�
		head = aux;
		// Hemos procesado el comando correctamente
		result = true;
	}
	return result;
}

//=======================================================================================================
	/** @fn			get(int*		key)
		@brief		Obtiene el value asociado a una key (si est� en la cach�).
		@return		Devuelve:
					<ol>
						<li>[n] = Value asociado a la clave (porque hubo un golpe de cach�)</li>
						<li>[-1] = Error de cach�, no se encontr� la key</li>
					</ol>
*/ //====================================================================================================
int LRUCache::get(int* key)
{
	struct nodoLista* aux = head;
	
	// Si hay datos en cach�...
	if (head != NULL)
	{	// Recorremos los nodos para buscar la key...
		do
		{	// Si la encontramos, devolvemos su value asociado
			if (aux->key == *key) return aux->value;
			// Si aun no hemos llegado a la cola de la cach�, pasamos al siguiente nodo
			if (aux != tail) aux = aux->pos;
			else return -1;
		}while(true);
	}
	return -1;
};

//###########################################################################################################
/** @file	main.cpp
	@brief	Fichero de la funci�n main, la cual es el punto de entrada a la ejecuci�n del programa.
	@author	PATRICIO J. MART�NEZ
	@date	26/01/20
*/ //########################################################################################################

#include "lrucache.h"															

#include <stdio.h>														// Librer�a para uso de funciones que tratan con la salida est�ndar de entrada/salida (mediante flujos)
#include <iostream>
using std::cout;
using std::endl;
#include <stdlib.h>														// uso de NULL, malloc,...
#include <string.h>														// strcmp,strtok,...
#include <ctype.h> 														// tolower

//===========================================================================================================
/**	@fn void main(int argc, char * argv[])
	@brief main	Inicio de ejecuci�n del software.
	@param[in]	argc	N� de elementos pasados como argumentos de entrada.
	@param[in]	argv	Vector de strings con los argumentos de entrada. Los argumentos son:
			<ol>
				<li>[0] = nombre del programa. Es [cacheOrenes.exe]</li>
				<li>[1] = N� de comandos de entrada que se introducir�n durante la ejecuci�n. Puede ser un valor entre 1 y 500000</li>
				<li>[2] = N� de elementos que es capaz de soportar la cach� (su capacidad). Puede ser un valor entre 1 y 1000.</li>
			</ol>
	@return	int		Valor entero asociado al tipo de finalizaci�n de la ejecuci�n. Los valores posibles son:
			<ol>
				<li>[ 0] = Operaci�n realizada correctamente</li>
				<li>[-1] = Fallo al introducir un n�mero incorrecto de par�metros</li>
				<li>[-2] = Fallo porque los par�metros introducidos no son v�lidos</li>
				<li>[-3] = Fallo producido por una Excepcion</li>
				<li>[-4] = Otros fallo internos durante la ejecuci�n</li>
			</ol>
*/
//========================================================================================================
int main(int argc, char *argv[])
{
	// Permite el uso de caracteres especiales (tildes, �,...) en comandos como printf, flujos de entrada/salida,...	
	setlocale(LC_CTYPE, "Spanish");
	
	cout << "\n   " << argv[0] << ":\t Ejecutando...\n" << endl;
	
	// Contendr� el c�digo de salida del programa
	int codExit = -4;

	// Si se introdujo el n� de par�metros correcto...
	if (argc == 3)
	{
		/** @brief	N� m�nimo de comandos que es posible aceptar */
		#define N_MIN	1
		/** @brief	N� m�ximo de comandos que es posible aceptar */
		#define N_MAX	500000
		/** @brief	Cantidad m�nima de elementos almacenables en la cach�  */
		#define M_MIN	1
		/** @brief	Cantidad m�xima de elementos almacenables en la cach�" */
		#define M_MAX	1000
		/** @brief	Valor m�nimo de "key" que aceptar�n los comandos */
		#define KEY_MIN	1
		/** @brief	Valor m�ximo de "key" que aceptar�n los comandos */
		#define KEY_MAX	20
		/** @brief	Valor m�nimo de "value" que aceptar�n los comandos */
		#define VALUE_MIN	1
		/** @brief	Valor m�ximo de "value" que aceptar�n los comandos */
		#define VALUE_MAX	2000

		// Almacenamos el valor de los par�metros para poder usarlos repetidamente y de manera c�moda
		int N = (int)strtol(argv[1], NULL, 10);
		int M = (int)strtol(argv[2], NULL, 10);

		// Si los par�metros introducidos son v�lidos...
		if(	(N_MIN <=N)	&&	(N<=N_MAX)	&&
			(M_MIN <=M)	&&	(M<=M_MAX)
		)
		{
			// Intentamos la operaci�n	
			try
			{	
				char* 		comando 			= (char*)malloc( 14 * sizeof(char) ); // El comando m�s largo que es posible introducir correctamente tiene s�lo 12 letras, a�adimos el RT
				char*		token 				= (char*)malloc( 14 * sizeof(char) );
				int 		key 				= -1;
				int 		value 				= -1;
				bool 		comandoProcesado	= false;	

				// se crea el objeto que contendr� la cach� LRU
				LRUCache* lRUCache = new LRUCache(&N);

				// Procesamos tantos comandos como indicamos en la ejecuci�n inicial del programa
				for (int i = 1; i <= N; i++)
				{
					do 
					{
						cout << "Ingresa el comando [" << i << "/" << N << "]: ";
						rewind(stdin);		// Vaciamos el buffer de entrada (para evitar posibles problemas con scanf por anteriores entradas de teclado capturadas)
						scanf("%[^\n]", comando);
						
						// Tomamos la porci�n del comando que nos indica el tipo
						token = strtok(comando, " ");

						// Si se trata de un comando SET
						if (!strcmp(token, "set"))					
						{
							// Tomamos el primer par�metro (si lo hay) y lo convertimos a entero para trabajar con �l
							token = strtok(NULL, " ");
							key = atoi(token);

							// Si es una "key" v�lida...
							if ( KEY_MIN <= key && key <= KEY_MAX )
							{
								// Tomamos el segundo par�metro (si lo hay) y lo convertimos a entero para trabajar con �l
								token = strtok(NULL, " ");
								value = atoi(token);

								// Si es un "value" v�lido...
								if ( VALUE_MIN <= value && value <= VALUE_MAX )
								{
									// Si no hay m�s par�metros, procesamos el comando
									if (!(token = strtok(NULL, " "))) comandoProcesado = lRUCache->set(&key, &value);
									else cout << "Comando incorrecto. Int�nt�lo de nuevo..." << endl;
								}else cout << "Comando incorrecto. Int�nt�lo de nuevo..." << endl;
							}else cout << "Comando incorrecto. Int�nt�lo de nuevo..." << endl;
						}

						// Si se trata de un comando GET
						else if (!strcmp(token, "get"))
						{
							// Tomamos el primer par�metro (si lo hay) y lo convertimos a entero para trabajar con �l
							token = strtok(NULL, " ");
							key = atoi(token);
	
							// Si es una "key" v�lida...
							if ( KEY_MIN <= key && key<= KEY_MAX )
							{
								// Si no hay m�s par�metros...
								if (!(token = strtok(NULL, " ")))
								{
									// Procesamos el comando
									value = lRUCache->get(&key);
									if (value != -1) cout << "El valor asociado a la clave " << key << " es: " << value << endl;
									else cout << "La clave " << key << " no existe en la cach�: -1" << endl;
									comandoProcesado = true;
								} else cout << "Comando incorrecto. Int�nt�lo de nuevo..." << endl;
							} else cout << "Comando incorrecto. Int�nt�lo de nuevo..." << endl;
						}else cout << "Comando incorrecto. Int�nt�lo de nuevo..." << endl;
	       			}while (comandoProcesado == false);
					comandoProcesado = false;	
				}
			}
			catch (char *str) 
			{	// Configuramos la salida indicando que hubo una excepci�n
				codExit = -3;
				cout << "\n   " << argv[0] << ":\t Aplicacion finalizada por fallo de excepcion. CodExit: " << codExit << "\n" << endl;
			}
		}else
		{
			codExit = -2;
			cout << "\n   " << argv[0] << ":\t Aplicacion finalizada porque los parametros introducidos son incorrectos. CodExit: " << codExit << "\n" << endl;
			cout << "\t\t El formato correcto de ejecuci�n es: \'" << argv[0] << " [N] [M]\'\n" << endl;
			cout << "\t\t Donde:\n" << endl;
			cout << "\t\t\t - [N] es el n� de comandos que se introducir�n y debe ser un valor entre 1 y 500000.\n" << endl;
			cout << "\t\t\t - [M] es la capacidad de la cach� y debe ser un valor entre 1 y 1000.\n" << endl;

		}
	}else
	{
		codExit = -1;
		cout << "\n   " << argv[0] << ":\t Aplicacion finalizada porque la cantidad de parametros introducidos es incorrecta. CodExit: " << codExit << "\n" << endl;
		cout << "\t\t El formato correcto de ejecuci�n es: \'" << argv[0] << " [N] [M]\'\n" << endl;
		cout << "\t\t Donde:\n" << endl;
		cout << "\t\t\t - [N] es el n� de comandos que se introducir�n y debe ser un valor entre 1 y 500000.\n" << endl;
		cout << "\t\t\t - [M] es la capacidad de la cach� y debe ser un valor entre 1 y 1000.\n" << endl;
	}
	return codExit;
}

//###########################################################################################################
/** @file	lrucache.h
	@brief	Fichero de cabecera de la clase LRUCache
	@author	PATRICIO J. MART�NEZ
	@date	26/01/2020
*/ //########################################################################################################
#ifndef _LRUCACHE_H_
#define _LRUCACHE_H_

#include "cache.h"															

//=======================================================================================================
/**	@class	LRUCache
	@brief	Clase que implementa el uso de una cach� de tipo LRU
*/ //====================================================================================================
class LRUCache: public Cache
{
// ------ CONSTANTES Y TIPOS ---------
	public: 			// Contenido p�blico (accesible desde esta clase, clases hijas y clases externas)
	//--------------------------------
	
	private:			// Contenido privado (accesible solo desde esta clase)
	//--------------------------------
	
	protected:		// Contenido protegido (accesible desde esta clase y clases hijas)
	//--------------------------------
	
// ---- VARIABLES -------
	public: 			// Contenido p�blico (accesible desde esta clase, clases hijas y clases externas)
	//--------------------------------
	
	private:			// Contenido privado (accesible solo desde esta clase)
	//--------------------------------
			
	protected:		// Contenido protegido (accesible desde esta clase y clases hijas)
	//--------------------------------


// ---- FUNCIONES -------
	public: 			// Contenido p�blico (accesible desde esta clase, clases hijas y clases externas)
		//=======================================================================================================
		/** @fn			LRUCache(	unsigned int N)
			@brief		Constructor
			@param[in]	N		Tama�o de la cach� (cantidad de claves que es capaz de almacenar)
		*/ //====================================================================================================
		LRUCache(int* N) :	Cache(N){};
		//=======================================================================================================
		/** @fn			~Cache()
			@brief		Destructor
		*/ //====================================================================================================
		~LRUCache()		{};

		bool		set(int* key, int* value);
		int			get(int* key);

	//--------------------------------
	
	private:			// Contenido privado (accesible solo desde esta clase)
	//--------------------------------

	protected:			// Contenido protegido (accesible desde esta clase y clases hijas)
	//--------------------------------
	
};

#endif //_LRUCACHE_H_

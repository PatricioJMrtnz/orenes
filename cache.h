//###########################################################################################################
/** @file	cache.h
	@brief	Fichero de cabecera de la clase Cache, la cual es una clase abstracta
	@author	PATRICIO J. MART�NEZ
	@date	26/01/2020
*/ //########################################################################################################
#ifndef _CACHE_H_
#define _CACHE_H_

#include <stdlib.h>													// uso de NULL
//=======================================================================================================
/**	@class	Cache
	@brief	Clase que implementa una cach� de n�meros naturales y facilita su uso.
*/ //====================================================================================================
class Cache
{
// ------ CONSTANTES Y TIPOS ---------
	public: 			// Contenido p�blico (accesible desde esta clase, clases hijas y clases externas)
	//--------------------------------
	
	private:			// Contenido privado (accesible solo desde esta clase)
	//--------------------------------
	
	protected:		// Contenido protegido (accesible desde esta clase y clases hijas)

		struct nodoLista
		{
   			int 				key;
   			int					value;
   			struct nodoLista*	pre;
   			struct nodoLista*	pos;
		};
	//--------------------------------


// ---- VARIABLES -------
	public: 			// Contenido p�blico (accesible desde esta clase, clases hijas y clases externas)
	//--------------------------------
	
	private:			// Contenido privado (accesible solo desde esta clase)
	//--------------------------------

	protected:		// Contenido protegido (accesible desde esta clase y clases hijas)

		/** @brief  N� de claves que se pueden llegar a almacenar en la cach� */
		int		cp;
		/** @brief  La �ltima clave introducida ha de mapearse al nodo en la lista de enlaces */
		bool	mp;
		/** @brief  Puntero a la cola de la cach� */
		nodoLista*	tail;
		/** @brief  Puntero a la cabeza de la cach� */
		nodoLista*	head;
	//--------------------------------

		
// ---- FUNCIONES -------
	public: 			// Contenido p�blico (accesible desde esta clase, clases hijas y clases externas)
		// Los CONSTRUCTORES es mejor definirlos en el .h para evitar durante el linkado problema de tipo --"undefined reference to XXX::XXX(X)"--

		//=======================================================================================================
		/** @fn			Cache(unsigned short M)
			@brief		Constructor
			@param[in]	M		N� de elementos m�ximo que contendr� la cach�
		*/ //====================================================================================================
		Cache(int* N)
		{
			// Inicializaci�n de variables
			cp = *N;
			mp = false;
			tail = NULL;
			head = NULL;
		};


		// Los DESTRUCTORES es mejor definirlos en el .h para evitar durante el linkado problema de tipo --undefined reference to "vtable for XXXX"--
		~Cache()		{};

		virtual bool		set(int* key, int* value) = 0;
		virtual int			get(int* key) = 0;
	//--------------------------------
	
	private:			// Contenido privado (accesible solo desde esta clase)
	//--------------------------------

	protected:			// Contenido protegido (accesible desde esta clase y clases hijas)
	//--------------------------------

};

#endif //_CACHE_H_
